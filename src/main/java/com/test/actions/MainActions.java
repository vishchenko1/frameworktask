package com.test.actions;

import com.test.base.BaseActions;
import com.test.base.BasePage;
import com.test.pages.Pages;
import com.test.pages.RozetkaPage;
import com.test.util.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import java.util.ArrayList;
import java.util.List;


import static com.test.base.BaseTest.driver;
import static com.test.pages.Pages.rozetkaPage;

public class MainActions extends BaseActions {
    private WebElement currentField;
    private WebElement currentButton;

  /*  public void openMainPage(){
        Reporter.log("Opening main page: " + Constants.ROZETKA_PAGE);
        driver.get(Constants.ROZETKA_PAGE);
    }*/

    public void clearSession() { driver.manage().deleteAllCookies(); }

    public void openPage(String url){
        Reporter.log("Opening page: " + url);
        driver.get(url);
    }

    private void checkField(WebElement a) throws Exception {
        if(!a.getText().equals("Apple iPhone X 64GB Silver, Полный комплект (M27)")){
            throw new Exception("Field has wrong text");
        }
    }
    private void checkCart(WebElement a) throws Exception {
        if(!a.getText().equals(" Корзина пуста ")){
            throw new Exception("Field has wrong text");
        }
    }
    private void waitForLoad(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void buyProduct(){
        WebElement currentField = driver.findElement(RozetkaPage.productNameBeforeBuying);
       try { checkField(currentField); }catch (Exception e) { e.printStackTrace(); }


        WebElement currentButton = driver.findElement(RozetkaPage.buyButton);
        currentButton.click();
        waitForLoad();

        currentField = driver.findElement(RozetkaPage.productNameInsideBuyMenu);
        try { checkField(currentField); }catch (Exception e) { e.printStackTrace(); }

        currentButton = driver.findElement(RozetkaPage.resumeBuyingButton);
        currentButton.click();
        waitForLoad();

        currentField = driver.findElement(RozetkaPage.productNameAfterBuying);
        try { checkField(currentField); }catch (Exception e) { e.printStackTrace(); }


    }

    public void checkCart(){
        currentButton = driver.findElement(RozetkaPage.cartButton);
        currentButton.click();
        waitForLoad();

        currentField = driver.findElement(RozetkaPage.productNameInCart);
        try { checkField(currentField); }catch (Exception e) { e.printStackTrace(); }

        currentButton = driver.findElement(RozetkaPage.removeFromCartButton);
        currentButton.click();
        waitForLoad();
        currentButton = driver.findElement(RozetkaPage.deleteWithoutSavingCartButton);
        currentButton.click();
        waitForLoad();

        currentField = driver.findElement(RozetkaPage.cartClearField);
        try { checkCart(currentField); }catch (Exception e) { e.printStackTrace(); }
        waitForLoad();
        
        currentButton = driver.findElement(RozetkaPage.cartCloseButton);
        currentButton.click();
        waitForLoad();

        currentField = driver.findElement(RozetkaPage.productNameAfterBuying);
        try { checkField(currentField); }catch (Exception e) { e.printStackTrace(); }

        driver.navigate().refresh();
        waitForLoad();



    }
    public void cartCheckAfterRefresh() {
        currentButton = driver.findElement(RozetkaPage.cartButton);
        currentButton.click();
        waitForLoad();

        currentField = driver.findElement(RozetkaPage.cartClearField);
        try { checkCart(currentField); }catch (Exception e) { e.printStackTrace(); }

        currentButton = driver.findElement(RozetkaPage.cartCloseButton);
        currentButton.click();
        waitForLoad();

    }







}
