package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import org.openqa.selenium.By;

public class RozetkaPage extends BasePage {
    //place for xpath

    //buttons
    public static final By buyButton = By.xpath("//div[contains(@class, 'detail-buy-btn-wrap ng-star-inserted')]//form");
    public static final By resumeBuyingButton = By.xpath("//div[contains(@class, 'cart-bottom ng-star-inserted')]//a");
    public static final By cartButton  = By.xpath("//li[contains(@class, 'header-actions__item header-actions__item_type_cart')]");
    public static final By removeFromCartButton = By.xpath("//div[contains(@class, 'cart-remove')]");
    public static final By deleteWithoutSavingCartButton = By.xpath("//div[contains(@class, 'cart-remove-popup-inner')]/a");
    public static final By cartCloseButton = By.xpath("//a[contains(@class, 'rz-popup-close')]");
    //fields
    public static final By productNameBeforeBuying = By.xpath("//div[contains(@class, 'content body-layout ng-star-inserted')]//h1");
    public static final By productNameInsideBuyMenu = By.xpath("//div[contains(@class, 'cart-content popup-content ng-star-inserted')]//div[contains(@class, 'purchase-inner clearfix')]/div/a");
    public static final By productNameAfterBuying = By.xpath("//div[contains(@class, 'detail-title-code pos-fix clearfix')]/h2");
    public static final By productNameInCart = By.xpath("//div[contains(@class, 'purchase-inner clearfix')]//a");
    public static final By cartClearField = By.xpath("//div[contains(@class, 'cart-dummy-inner h1')]");



}
